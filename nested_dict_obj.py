"""
Problem 2: Write a function to print all keys with their depth from a nested dictionary. It should be able to handle
a Python object in addition to a dictionary from Question 1
"""

class Person(object):
    def __init__(self, first_name, last_name, father):
        self.first_name = first_name
        self.last_name = last_name
        self.father = father

def print_depth(d):
    stack = [(d, list(d.keys()))]

    while stack:
        cur, keys = stack.pop()

        while keys:

            k, keys = keys[0], keys[1:]

            print(k, len(stack) + 1)

            v = cur[k]

            if isinstance(v, dict):
                stack.append((cur, keys))
                stack.append((v, list(v.keys())))
                break

            if isinstance(v, Person):
                stack.append((cur, keys))
                stack.append((v.__dict__, list(v.__dict__.keys())))
                break


if __name__ == "__main__":
    person_a = Person("User", "1", None)
    person_b = Person("User", "2", person_a)
    a = {
        "key1": 1,
        "key2": {
            "key3": 1,
            "key4": {
                "key5": 4,
                "user": person_b
            }
        }
    }


    print_depth(a)
