""""
Problem 1: Write a function to Printing all keys with their depth from a nested dictionary
"""
def print_depth(d):
    # Store dict in a stack
    stack = [(d, list(d.keys()))]
    depth = {}
    while stack:
        cur, keys = stack.pop()
        while keys:
            k, keys = keys[0], keys[1:]
            depth[k]= len(stack)+1
            v = cur[k]
            if isinstance(v, dict):
                 stack.append((cur, keys))
                 stack.append((v, list(v.keys())))
                 break
    return depth


if __name__ == "__main__":

    a = {
        "key1": 1,
        "key2": {
            "key3": 1,
            "key4": {
                "key5": 4
            }
        }
    }


    [print(key, value) for key, value in sorted(print_depth(a).items(), key=lambda item: item[0])]