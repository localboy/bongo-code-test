import unittest

from lca import make_tree, Solution
from nested_dict import print_depth


class TestPrintDepth(unittest.TestCase):
    """
    Test for problem 1: Write a function to print all keys with their depth from a nested dictionary
    """

    def setUp(self):
        pass

    def test_print_depth(self):
        a = {
            "key1": 1,
            "key2": {
                "key3": 1,
                "key4": {
                    "key5": 4
                }
            }
        }

        lca = {'key1': 1, 'key2': 1, 'key3': 2, 'key4': 2, 'key5': 3}

        self.assertEqual(print_depth(a), lca)


class TestLca(unittest.TestCase):
    """
    Test for problem 3: Write a function to find Least Common Ancestor and print its value from 
    a Node which accept two nodes as paramenter.
    """
   
    def setUp(self):   
        self.ob1 = Solution()
        self.tree = make_tree([1, 2, 3, 4, 5, 6, 7, 8, 9])

    def test_lca(self):
        self.assertEqual(self.ob1.lca(self.tree, 6, 7).data, 3)
        self.assertEqual(self.ob1.lca(self.tree, 3, 7).data, 3)


if __name__ == "__main__":
   unittest.main()