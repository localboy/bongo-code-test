"""
Problem 3: Write a function to find Least Common Ancestor and print its value from 
a Node which accept two nodes as paramenter.
"""

class Node:
   # Constractor to creaete a new binary node
   def __init__(self, data, left = None, right = None):
      self.data = data
      self.left = left
      self.right = right


def insert(temp,data):
   # Constracting Node tree
   que = []
   que.append(temp)
   while (len(que)):
      temp = que[0]
      que.pop(0)
      if (not temp.left):
         if data is not None:
            temp.left = Node(data)
         else:
            temp.left = Node(0)
         break
      else:
         que.append(temp.left)
         if (not temp.right):
            if data is not None:
               temp.right = Node(data)
            else:
               temp.right = Node(0)
            break
         else:
            que.append(temp.right)


def make_tree(elements):
   # Creating tree from elements
   
   Tree = Node(elements[0])
   for element in elements[1:]:
      insert(Tree, element)
   return Tree


class Solution(object):
   # Finding Least Common Acnestor
   def lca(self, root, p, q):
      if not root:
         return None
      if root.data == p or root.data ==q:
         return root
      left = self.lca(root.left, p, q)
      right = self.lca(root.right, p, q)
      if right and left:
         return root
      return right or left
  

if __name__ == "__main__":
   
   ob1 = Solution()
   tree = make_tree([1, 2, 3, 4, 5, 6, 7, 8, 9])

   print(ob1.lca(tree, 6, 7).data)
   print(ob1.lca(tree, 3, 7).data)

   